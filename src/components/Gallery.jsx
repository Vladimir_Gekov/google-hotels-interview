import React, {useState,useEffect} from "react";
import Slider from "react-slick";
import "./Gallery.scss";
import { ToastContainer } from "react-toastr";

export default function Gallery(props) {
  const [toastr, setToastr] = useState();
  const notify = () => {
    toastr.success("Your reservation has been confirmed.");
    setTimeout(() => {
      toastr.clear();
    }, 3000);
  };
  const [showGallery, setShowGallery] = useState();
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  const [slider, setSlider] = useState();
  useEffect(() => {
    const ind = props.locations.findIndex((el) => el.selected);
    if (ind >= 0) {
      goTo(ind);
    }
  }, [props.locations]);
  const goTo = function (index) {
    setShowGallery(true);
    slider.slickGoTo(index);
  };
  return (
    <>
      <ToastContainer
        className="toast-top-right"
        ref={(ref) => {
          setToastr(ref); }}/>
      <div className={showGallery ? "" : "gallery-hidden"}>
        {props.locations ? (
          <>
            <Slider
              {...settings}
              ref={function (c) {
                setSlider(c);
              }}>
              {props.locations.map((location, i) => (
                <div className="container" key={i}>
                  <div className="box">
                    <div className="box__info">
                      <div className="box__info-image">
                        <img className="img" src="hotelroom.jpg" />
                      </div>
                      <div className="box__info-text">
                        <h3 className="title">Name: {location.name}</h3>
                        <p className="address">Address: {location.vicinity}</p>
                        <p className="rating">Rating: {location.rating}</p>
                        <p className="design">Design may vary</p>
                      </div>
                    </div>
                      <div className="button-container">
                    <button className="button" onClick={notify}>BOOK</button>
                      </div>
                  </div>
                </div>
              ))}
            </Slider>
          </>
        ) : null}
      </div>
    </>
  );
}