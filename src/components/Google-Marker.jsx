/*global google*/
import React, { useState } from "react";
import { Marker } from "react-google-maps";

const GoogleMarker = ({
  i,
  place,
  setActiveFalse,
  setFalseCurrent,
  showHotel,
}) => {
  const [active, setActive] = useState(false);

  setActiveFalse(() => {
    setActive(false);
  });

  function clickHandler() {
    showHotel(i);
    setFalseCurrent(i);
    setActive(true);
  }
  return (
    <Marker
      onClick={clickHandler}
      icon={{
        url: active ? "home-icon-active.svg" : "home-icon.svg",
        scaledSize: active
          ? new google.maps.Size(48, 48)
          : new google.maps.Size(32, 32),
      }}
      position={{
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
      }}
    />
  );
};

export default GoogleMarker;