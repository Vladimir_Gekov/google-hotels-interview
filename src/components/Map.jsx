/*global google*/
import React, { useState, useEffect } from "react";
import {
  compose,
  withProps,
  withHandlers,
  withState,
  withStateHandlers,
} from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";
import GoogleMarker from "./Google-Marker";

let infoWindow;
// ! Add key here
const key = process.env.REACT_APP_KEY;
const Map = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${key}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100vh` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,

  withStateHandlers(
    () => ({
      iconUrl: "home-icon.svg",
      iconSize: new google.maps.Size(32, 32),
    }),
    {
      onMarkerClick: () => () => ({
        iconUrl: "home-icon-active.svg",
        iconSize: new google.maps.Size(48, 48),
      }),
    }
  ),

  withState("places", "updatePlaces", ""),
  withHandlers(() => {
    const refs = {
      map: undefined,
    };

    return {
      onMapMounted: () => (ref) => {
        refs.map = ref;
        infoWindow = new google.maps.InfoWindow();
      },
      fetchPlaces: ({ updatePlaces }) => {
        const bounds = refs.map.getBounds();
        const service = new google.maps.places.PlacesService(
          refs.map.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED
        );
        const request = {
          bounds: bounds,
          type: ["lodging"],
        };
        service.nearbySearch(request, (results, status) => {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            updatePlaces(results);
          }
        });
      },
    };
  })
)((props) => {
  props.setLocations(
    props.places
      ? props.places.map((el) => {
          return { ...el, selected: false };
        })
      : []
  );
  const setActiveCallbacks = [];
  function showHotel(ind) {
    console.log(props.places);
    props.setLocations(
      [...props.places].map((el, index) => {
        ind === index ? (el.selected = true) : (el.selected = false);
        return el;
      })
    );
  }

  function setActiveMarkers(ind) {
    setActiveCallbacks.forEach((cb, i) => {
      if (ind !== i) {
        cb();
      }
    });
  }

  return (
    <GoogleMap
      onTilesLoaded={props.fetchPlaces}
      ref={props.onMapMounted}
      onBoundsChanged={props.fetchPlaces}
      defaultZoom={15}
      defaultCenter={props.center}
    >
      {props.places &&
        props.places.map((place, i) => (
          <GoogleMarker
            showHotel={showHotel}
            place={place}
            setFalseCurrent={(i) => setActiveMarkers(i)}
            setActiveFalse={(cb) => setActiveCallbacks.push(cb)}
            key={i}
            i={i}
          />
        ))}
    </GoogleMap>
  );
});

const MyPosition = (props) => {
  const [center, setCenter] = useState({});
  useEffect(() => {
    navigator?.geolocation.getCurrentPosition(
      ({ coords: { latitude: lat, longitude: lng } }) => {
        const pos = { lat, lng };
        setCenter(pos);
      }
    );
  }, []);
  return <Map center={center} setLocations={props.setLocations} />;
};

export default MyPosition;
