import React from "react";
import Map from "../components/Map";
import Gallery from "../components/Gallery";
import "./Home.scss";
import {useState} from "react";

export default function Home() {
    const [locations, setLocations] = useState([])
  return (
    <section className="home-screen">
      <div className="header">
        <img className="header__logo" src="stanga_logo.svg"></img>
        <img className="header__burger-icon" src="burger-icon.svg"></img>
      </div>

      <div className="primary">
        <div className="map">
          <Map setLocations={setLocations} locations={locations}/>
        </div>
        <div className="slider">
            <Gallery locations={locations}/>
        </div>
      </div>
    </section>
  );
}