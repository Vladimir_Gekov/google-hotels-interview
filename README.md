### Setting up the project

Run `yarn` or `npm i` to install dependencies

Before running the next command make sure to add api key for google places api in Map.jsx

Create `.env` file in the root of the project with the following structure:
`REACT_APP_KEY="insert_your_google_maps_api_key_here"`.

Run `yarn start` or `npm run start` to start the server

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.